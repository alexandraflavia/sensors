import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Dashboard } from './containers/dashboard.component';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';

const routes = [
  { path: '', component: Dashboard },
]

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), MatGridListModule],
  declarations: [Dashboard],
})
export class RecordingModule {}
