module.exports = {
  projects: [
    '<rootDir>/apps/sensors',
    '<rootDir>/libs/users',
    '<rootDir>/libs/recording',
    '<rootDir>/libs/replay',
  ],
};
