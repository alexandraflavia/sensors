import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'record',
    loadChildren: () => import('@sensors/recording').then(m => m.RecordingModule),
  },
  {
    path: 'replay',
    loadChildren: () => import('@sensors/replay').then(m => m.ReplayModule),
  },
  {
    path: 'users',
    loadChildren: () => import('@sensors/users').then(m => m.UsersModule),
  },
];

export const menu: any[] = [
  { link: 'record', label: 'Record', icon: 'camera' },
  { link: 'replay', label: 'Events', icon: 'timeline' },
  { link: 'users', label: 'Users & Friends', icon: 'people' },
];
